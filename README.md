# Description
You need to create a component for creating tables with additional functionality for users.

For demonstration of component, you should create a page where user can choose product category (checkboxes on the left).
On product category checkbox selection data should be loaded, and displayed in a table.

On click on table column title, all table data should be sorted ASC (first click!) and on second click data should be sorted DESC.

### Data format:
```
[{
    id: 1,
    type: "phone",
    name: “iPhone 5”,
    price: “$400”,
    rating: 3.8
}, {
    id: 2,
    type: "phone",
    name: “Nokia Lumia 1320”,
    price: “$130”,
    rating: 2.1
}]
```

# Conditions of execution:
1. Task should be done using ReactJS and ES6 syntax.
2. You can add any additional functionality for better user experience if you want.
