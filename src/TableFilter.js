import React from 'react';
import PropTypes from 'prop-types';
import FilterItem from './FilterItem';

class TableFilter extends React.Component {
    constructor(props) {
        super(props);
        
        this.filterItems = this.props.filterItems;
    }

    shouldComponentUpdate(nextProps) {
        if(this.filterItems.length !== nextProps.filterItems.length) {
            this.filterItems = nextProps.filterItems;
            return true;
        }
        return false

    }

    render() {
        return (
            <div className="TableFilter__wrapper">
                {
                    this.filterItems.map( (item, i) => 
                        <FilterItem item={item} 
                                    key={i}
                                    onItemChange={this.props.handleFilterChange}
                        />
                    )
                }
            </div>
        );
    }

}

export default TableFilter;

TableFilter.propTypes = {
    filterItems: PropTypes.array,
    handleFilterChange: PropTypes.func,
}